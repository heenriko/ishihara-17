'use-strict';

let obs;
let testFinished = false;

$(function()
{



let taskIndex = 0;        // Keeps track of task index during the test.
let testStart = false;    // i/o test start finished.

// Define ishihara data:
let ishihara = 
[
  {id: 1,  img: "img/1.png",  normal: 12,   defic: 12,   cBlind: 12 },
  {id: 2,  img: "img/2.png",  normal: 8,    defic: 3,    cBlind: null},
  {id: 3,  img: "img/3.png",  normal: 29,   defic: 70,   cBlind: null },
  {id: 4,  img: "img/4.png",  normal: 5,    defic: 2,    cBlind: null},
  {id: 5,  img: "img/5.png",  normal: 3,    defic: 5,    cBlind: null},
  {id: 6,  img: "img/6.png",  normal: 15,   defic: 17,   cBlind: null},
  {id: 7,  img: "img/7.png",  normal: 74,   defic: 21,   cBlind: null},
  {id: 8,  img: "img/8.png",  normal: 6,    defic: null, cBlind: null},
  {id: 9,  img: "img/9.png",  normal: 45,   defic: null, cBlind: null},
  {id: 10, img: "img/10.png", normal: 5,    defic: null, cBlind: null},
  {id: 11, img: "img/11.png", normal: 7,    defic: null, cBlind: null},
  {id: 12, img: "img/12.png", normal: 16,   defic: null, cBlind: null},
  {id: 13, img: "img/13.png", normal: 73,   defic: null, cBlind: null},
  {id: 14, img: "img/14.png", normal: null, defic: 5,    cBlind: null },
  {id: 15, img: "img/15.png", normal: null, defic: 45,   cBlind: null },
  {id: 16, img: "img/16.png", normal: 26,   defic: {protan: 6, deutan: 2}, cBlind: null},
  {id: 17, img: "img/17.png", normal: 42,   defic: {protan: 2, deutan: 4}, cBlind: null}
];

/**
 * Class Observer (Singelton pattern)
 * @prop {*ARRAY}  tasks      - All Ishihara tasks.
 * @prop {*STRING} vision     - Color vision label (e.g: Normal, Protanopia etc.). 
 * @prop {*FLOAT}  visionPerc - Percentage of the highest score among the categories.
 * @method addTask(answer)    - Add task.
 */
let Observer = function()
{
  this.tasks = [];      // Array to store tasks.
  this.vision = "";     // String to determine color vision.
  this.degree = "";
  this.perc = 0;
  this.postEvaluation = "Not applicable";

  /**
   * Add task
   * @description Push task into tasks array:
   * @param {*INT || *NULL} answer 
   */
  this.addTask = function(answer)
  {
    this.tasks.push({'answer': answer, 'extra': null });
  }
 

  /**
   * Add task
   * @description Push task into tasks array:
   * @param {*INT || *NULL} answer 
   */
  this.validateVision = function()
  { 
    let normal = 0;
    let protan = 0;
    let deutan = 0;
    let fail = 0;

    // Set score:
    for(let i = 0; i < this.tasks.length; i++)
    {
      if(i < this.tasks.length-2) // First 15:
      {
        if(i != 0)
        {
          switch(this.tasks[i].answer)    
          {
            case ishihara[i].normal:  normal ++; break;  
            case ishihara[i].defic:   protan ++; deutan ++; break;  
            default:                  fail ++; break; 
          }
        }
      }
      else                        // Two last expections.
      {
        switch(this.tasks[i].answer)
        {
          case ishihara[i].normal:        normal ++; break;  
          case ishihara[i].defic.protan:  protan ++; break;  
          case ishihara[i].defic.deutan:  deutan ++; break; 
          default:                        fail ++;   break; 
        }
      } 
    }

    // Calc percentage:
    let normalP = parseFloat(((normal / (this.tasks.length-1)) * 100).toFixed(2));
    let protanP = parseFloat(((protan / (this.tasks.length-1)) * 100).toFixed(2));
    let deutanP = parseFloat(((deutan / (this.tasks.length-1)) * 100).toFixed(2));
    let failP   = parseFloat(((fail / (this.tasks.length-1)) * 100).toFixed(2));
    

    // Calc based on score:
    if(normal >= protan && normal >= deutan && normal >= fail)
    {
      // Good vision:
      if(normal >= 10)
      {
        this.vision = "Normal";
        this.degree = calcDegree(normalP);
        this.perc = normalP;
      }  
      // 9 and less is regarded as deficient.
      else
      {
        this.vision = "Weak normal";
        this.postEvaluation = postEval(this.tasks);
        this.degree = calcDegree(normalP);
        this.perc = normalP;
      }
    }
    // Protan:
    else if(protan > deutan && protan > fail)
    {
      this.vision = "Protanopia";
      this.degree = calcDegree(protanP);
      this.perc = protanP;
    }
    // Deutan:
    else if(deutan > protan && deutan > fail)
    {
      this.vision = "Deuteranopia";
      this.degree = calcDegree(deutanP);
      this.perc = deutanP;
    }
    // Protan equals Deutan
    else if (protan >= fail || deutan >= fail && protan == deutan)
    {
      this.vision = "Protanopia or Deuteranopia";
      this.postEvaluation = postEval(this.tasks);
      this.degree = calcDegree(protanP);
      this.perc = protanP;
       
    }
    // Fail:
    else if (fail > deutan && fail > protan && fail > normal)
    {
      this.vision = "Color blind";
      this.postEvaluation = postEval(this.tasks);
      this.degree = calcDegree(failP);
      this.perc = failP;
    }

    function calcDegree(perc)
    {
      if      (perc <= 25)                return "None";
      else if (perc >= 25 && perc <= 50)  return "Weak";
      else if (perc >= 50 && perc <= 75)  return "Moderate";
      else if (perc >= 75 && perc <= 100) return "Strong";
    }

    function postEval(tasks)
    {
      if( tasks[15].answer == ishihara[15].defic.protan || tasks[16].answer == ishihara[16].defic.protan )
      {
        return "Protanopia";
      }
        
      else if ( tasks[15].answer == ishihara[15].defic.deutan || tasks[16].answer == ishihara[16].defic.deutan )
      {
        return "Deuteranopia";
      }
      else if(tasks[15].answer == ishihara[15].normal)
      {
        if(tasks[15].extra == ishihara[15].defic.protan)
          return "Protanomalia";
        else if(tasks[15].extra == ishihara[15].defic.deutan)
          return "Deuteranomalia";
      }
      else
        return "Not Applicable";
    }

    console.log('Normal\n  score: ' + normal + '\n  perc:  ' + normalP + '%');
    console.log('Protan\n  score: ' + protan + '\n  perc:  ' + protanP + '%');
    console.log('Deutan\n  score: ' + deutan + '\n  perc:  ' + deutanP + '%');
    console.log('Fail\n  score: ' + fail   + '\n  perc:  ' + failP + '%');

  } // End validation.
} // End Class Observer.

obs = new Observer();   // Create new observer instance, Singe Pattern.

/**
 * Start Test
 * @description Initiated from start button
 */
function startTest()
{
  // Set Image:
  $('#ishImg').attr('src', ishihara[taskIndex].img);

  // Change display:
  $('#info').css('display','none');
  $('#tasksContainer').css('display','block');

  testStart = true;
}

/**
 * Start Test
 * @description Initiated from start button
 */
function nextTask()
{
  //console.log(obs.tasks)
  let nextButton = $('#taskNextButton');                          // Cache next button. 
  let label = $('#labelIshNum');                                  // Cache ishihara number label. 
  let num = $('#ishNum');                                         // Cache ishihara number. 

  num.focus();                                                    // Set focus to input element.
  let taskVal = (num.val() != "") ? parseInt(num.val()) : null;   // Get number value.
 
  obs.addTask(taskVal);                    // Add task to observer object. 
  num.val("");                                    // Reset value.

  if(taskIndex < ishihara.length-1 || taskIndex == 16)
  {
    // console.log('index: '+ taskIndex);
    //Post test at plate 15 if read normally:
    if(taskIndex == 15 && obs.tasks[15].answer == ishihara[15].normal && obs.tasks[15] != null)
    {
      console.log('what');
      num.css('display','none');
      nextButton.css('display','none');

      label.text('which numeral is clearer? (2 or 6)');
      
      $('#extraButton1').text('2');
      $('#extraButton2').text('6');

      $('.extraButton').css('display','inline-block');

      console.log('post test 15'); 
    }
    //Post test at plate 16 if read normally:
    else if(taskIndex == 16 && obs.tasks[16].answer == ishihara[16].normal && obs.tasks[16] != null)
    {
      num.css('display','none');
      nextButton.css('display','none');

      label.text('which numeral is clearer? (4 or 2)');
      
      $('#extraButton1').text('4');
      $('#extraButton2').text('2');

      $('.extraButton').css('display','inline-block');

      console.log('post test 16'); 
    }
    else
    {
      if(taskIndex == 16)
        results();
      else
      {
        taskIndex++;                                        // increment task index.
        $('#ishImg').attr('src', ishihara[taskIndex].img);  // Update image.
      }
      
    }
    
  }
  else
    results();
  
  if(taskIndex >= ishihara.length-1)                
    $('#taskNextButton').text('finish');

  //obs.validateVision();
}

function results()
{
  exitFullscreen();
  
  // Test is finished at this point:
  testStart = false;

  // Validate vision:
  obs.validateVision();

  // Cache results element:
  let results = $('#results');  
  
  // Change display:
  $('#tasksContainer').css('display','none');
  results.css('display','block');

  // Set results:

  results.find('p').html(`
    Color vision: <span class="resultVal" style="padding-left: 4em;">${obs.vision}</span><br>
    Post-eval for vision:  <span class="resultVal" style="padding-left: 0.85em;">${obs.postEvaluation}</span><br>
    Degree: <span class="resultVal" style="padding-left: 6em;">${obs.degree}</span> (${obs.perc}%)<br>
    `);
    
  
  testFinished = true;
  console.log(obs);
  
}

/*------------------------------------------------*/

let normal = [12, 8, 29, 5, 3, 15, 74, 6, 45, 5, 7, 16, 73, null, null, 26, 42];

let protan = [12, 3, 29, 5, 3, 15, 74, 6, 45, 5, 7, null, null, 5, 45, 6, 2];

let deutan = [12, 3, 70, 2, 5, 17, 21, null, null, null, null, null, null, 5, 45, 2, 4];

function test(vis)
{
  for(var i=0; i<vis.length; i++)
  {
    obs.addTask(vis[i]);
  }

  results();      
}

function fullscreen(elem)
{
  if (elem.requestFullscreen) {
      elem.requestFullscreen()
  } else if (elem.msRequestFullscreen) {
      elem.msRequestFullscreen()
  } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen()
  } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen()
  }   
}

function exitFullscreen() 
{
  if (document.exitFullscreen) {
        document.exitFullscreen()
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen()
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen()
    }
}

$(document).ready(function()
{
  $('#startTest').on('click',function()
  {
    startTest();
    fullscreen(document.documentElement);
  });

  $('#taskNextButton').on('click',function()
  {
    nextTask();
  });

  $('.extraButton').on('click',function()
  {
    obs.tasks[taskIndex].extra = parseInt( $(this).text() );
    
    $('#ishNum').css('display','block');
    $('#taskNextButton').css('display','block');  
    $('.extraButton').css('display','none');  
    $('#labelIshNum').text('enter number');
    
    if(taskIndex == 15)
    {
      $('#ishNum').focus();
      
      taskIndex++;                                        // increment task index.
      $('#ishImg').attr('src', ishihara[taskIndex].img);  // Update image.
    }
    else
    {
      results();
    }
  });

  /**
   * Temporarily test data 
   */
  $('#autoTest').on('click',function()
  {
    test(protan);
  });

  // Press Key when input is focused:
  $('input').keypress(function(event)
  {
    //event.preventDefault();

    //Hit 'enter' button:
    if(event.which == 13) 
    {
      if(testStart)
        nextTask();
      else
        console.log('test has not started or is finshed');
    } 
  });
});

}); // End closure.
